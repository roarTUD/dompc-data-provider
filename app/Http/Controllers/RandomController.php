<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;
use Faker\Factory;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class RandomController extends Controller
{
    protected $faker;

    public function __construct()
    {
        $this->faker = Factory::create();
    }

    public function getFloat()
    {
        return response()->json($this->randomFloat());
    }

    public function getNumber()
    {
        return response()->json($this->randomNumber());
    }

    public function getArray()
    {
        return response()->json($this->randomArray(request()->get('length', 10)));
    }

    public function getSeries()
    {
        $arr = [];

        for($i = 0; $i <= request()->get('length', 10); $i++) {
            $step = request()->get('start', Carbon::now()->timestamp) + $i*100;
            $arr[$step] = $this->randomFloat();
        }

        return response()->json($arr);
    }

    public function getSensor()
    {
        return response()->json([
            'past' => $this->randomArray(request()->get('length_past', 25)),
            'current' => $this->randomFloat(),
            'prediction' => $this->randomArray(request()->get('length_prediction', 5)),
        ]);
    }

    protected function randomNumber()
    {
        return $this->faker->numberBetween(request()->get('min'), request()->get('max'));
    }

    protected function randomFloat()
    {
        return $this->faker->randomFloat(request()->get('decimals'), request()->get('min'), request()->get('max'));
    }

    protected function randomArray($length = 10)
    {
        $arr = [];

        for($i = 0; $i <= $length; $i++) {
            $arr[] = $this->randomFloat();
        }

        return $arr;
    }
}
